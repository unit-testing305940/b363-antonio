function factorial(n) {
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

// ACTIVITY #2 //

// INSTRUCTIONS //
// 2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
	// a. If the number received is divisible by 5, return true.
	// b. If the number received is divisible by 7, return true.
	// c. Return false if otherwise

function div_check(n) {
	if(n % 5 === 0) return true;
	if(n % 7 === 0) return true;
	return false;
}

module.exports = {
	factorial: factorial,
	div_check: div_check
};