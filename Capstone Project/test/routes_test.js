const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:8001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:8001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('test_api_post_currency_returns_200_if_complete_input', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': 'United States Dollar',
        	'alias' : 'USD',
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		}
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        })
    })

	it('test_api_post_currency_returns_400_if_no_name', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': 'United States Dollar',
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		}
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_name_is_not_string', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': 898,
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		}
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_name_is_empty', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': "",
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		}
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_no_ex', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': 'United States Dollar',
      		'alias': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		}
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_ex_is_not_object', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': "US Dollar",
      		'ex': "hi"
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': "US Dollar",
      		'ex': ""
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_no_alias', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': 'United States Dollar',
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		},
    		'hello': 'hi'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_alias_is_not_string', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
        	'name': "US Dollar",
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		},
    		'alias': 777
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

 	it('test_api_post_currency_returns_400_if_duplicate_alias', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		},
            'alias': 'USD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    })

    it('test_api_post_currency_returns_200_if_no_dupes', (done) => {
        chai.request('http://localhost:8001')
        .post('/currency')
        .type('json')
        .send({
    		'name': "South Korean Won",
    		'alias': 'MMM',
      		'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
    		}
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        })
    })


})
