const { exchangeRates, hasDuplicateAlias } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {

		if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'No currency name'
            })
        }

        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Name should consist of letters only.'
            })
        }

        if(typeof req.body.name === null){
            return res.status(400).send({
                'error': 'No name given'
            })
        }

        if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'No currency exchange.'
            })
        }

        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Ex should be an object.'
            })
        }

        if(typeof req.body.ex === null){
            return res.status(400).send({
                'error': 'No ex given'
            })
        }

        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'No currency alias'
            })
        }

        if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Alias should consist of letters only.'
            })
        }

		if(req.body.hasOwnProperty('name') && req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('ex')){
            return res.status(200).send({
                'success': 'Complete input given'
            })
        }

        if(hasDuplicateAlias(req.body.alias) === true){
        	return res.status(400).send({
                'error': 'Duplicate Alias'
            })
        }
        
        if(hasNoDuplicateAlias(req.body.alias) === false){
        	return res.status(200).send({
                'success': 'No Duplicate Alias'
            })
        }
	})


}