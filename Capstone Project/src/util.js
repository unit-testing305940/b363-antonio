const exchangeRates = {
    'usd': {
      	'name': 'United States Dollar',
      	'alias': 'USD',
      	'ex': {
        	'peso': 50.73,
        	'won': 1187.24,
        	'yen': 108.63,
        	'yuan': 7.03
    	}
    },
    'yen': {
      	'name': 'Japanese Yen',
      	'alias': 'JPY',
      	'ex': {
	        'peso': 0.47,
	        'usd': 0.0092,
	        'won': 10.93,
	        'yuan': 0.065
      	}
    },
    'peso': {
      	'name': 'Philippine Peso',
      	'alias': 'PHP',
      	'ex':{
	        'usd': 0.020,
	        'won': 23.39,
	        'yen': 2.14,
	        'yuan': 0.14
      	}
    },
    'yuan': {
      	'name': 'Chinese Yuan',
      	'alias': 'CNY',
      	'ex': {
	        'peso': 7.21,
	        'usd': 0.14,
	        'won': 168.85,
	        'yen': 15.45
      	}
    },
    'won': {
      	'name': 'South Korean Won',
      	'alias': 'KRW',
      	'ex': {
	        'peso': 0.043,
	        'usd': 0.00084,
	        'yen': 0.092,
	        'yuan': 0.0059
      	}
    }
};

function hasDuplicateAlias(alias) {
	Object.values(exchangeRates).forEach( val => {
		if (val.alias === alias) {
			return true;
		}
	});
	return false;
}

module.exports = {
	exchangeRates: exchangeRates,
	hasDuplicateAlias: hasDuplicateAlias
}

// Requirements for Capstone Project:
// 1. Check if post /currency is running
// 2. Check if post /currency returns status 400 if name is missing
// 3. Check if post /currency returns status 400 if name is not a string
// 4. Check if post /currency returns status 400 if name is empty
// 5. Check if post /currency returns status 400 if ex is missing
// 6. Check if post /currency returns status 400 if ex is not an object
// 7. Check if post /currency returns status 400 if ex is empty
// 8. Check if post /currency returns status 400 if alias is missing
// 9. Check if post /currency returns status 400 if alias is not an string
// 10. Check if post /currency returns status 400 if alias is empty
// 11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
// 12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates

// Additional Instructions:
// Clone the boilerplate (s5) from the resources folder
// Use npm install to reinstall node_modules
// Remove remote connection to online repo using git remote remove origin or by deleting the .git folder from the clone.
// Create a new online repo called s5
// Initialize your cloned boilerplate, add the remote link to your s5 repo and push to git with the commit message of “Add activity s5".
// Add the link in Boodle.