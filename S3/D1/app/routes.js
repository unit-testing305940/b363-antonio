const { names } = require('../src/util.js')

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

// ACTIVITY #1 //

// 1. Add a another if statement in the /users post route that returns and sends 400 status and error message if the request body does not have a username field. //

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }

        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }

        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }

        if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter USERNAME'
            })
        }
    })
}
