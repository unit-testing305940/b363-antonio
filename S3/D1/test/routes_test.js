const chai = require('chai');

const expect = chai.expect;
//same as const {expect} = require("chai");

const http = require('chai-http');
chai.use(http)

describe("api_test_suite", () => {

	it("test_api_people_is_running", () => {
		chai.request('http://localhost:5001')
		// "get()" specifies the type of HTTP request
		// accepts the API endpoint
		.get('/people')
		// "end()" is the method that handles the error and response that will be received from the endpoint
		.end((err,res) => {
			// ".not" negates all assertions that follow in the chain 
			expect(res).to.not.equal(undefined);
		});
	});

	it("test_api_people_returns_200", (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			// "done()" function is typically used in asynchronous test cases to signal that the test case is complete
			done();
		}) 
	});
 
  	//name can be as long as 3 lines, better to describe what you are testing
	it("test_api_post_person_returns_400_if_no_person_name", (done) => {
		chai.request('http://localhost:5001')
		// ".post()" specifies the type of http request
		// accepts the API endpoint
		.post('/person')
		// "type" specifies the type of input to be sent out as part of the POST request
		.type('json')
		// "send()" specifies the data to be sent as part of the POST request
		// request body
		.send({
			alias: "Jason",
			age: 28
		})
		.end((err,res) => {
			expect(res.status).to.equal(400);
			done();
		})
	});

// ACTIVITY #2 //
// 2. In your test.js, Add following test cases in the api_test_suite_users:
	// a. Check if the post endpoint is running
	// b. Check if the post endpoint encounters an error if there is no username
	// c. Check if the post endpoint encounters an error if there is no age 

	it("test_api_post_person_is_running", (done) => {
		chai.request('http://localhost:5001')
		.get('/person')
		.end((err,res) => {
			expect(res).to.not.equal(undefined);
			done()
		});
	});

	it("test_api_post_person_returns_400_if_no_USERNAME", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Mimiyuuh",
			name: "Mimi",
			age: 32
		})
		.end((err,res) => {
			expect(res.status).to.equal(400);
			done();
		})
	});


	it("test_api_post_person_returns_400_if_no_AGE", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Mimiyuuh",
			name: "Mimi",
			birthday: "04/11/1999"
		})
		.end((err,res) => {
			expect(res.status).to.equal(400);
			done();
		});
	});


});

// ACTIVITY INSTRUCTIONS //

// (DONE) 1. Add a another if statement in the /users post route that returns and sends 400 status and error message if the request body does not have a username field.
// 2. In your test.js, Add following test cases in the api_test_suite_users:
	// (DONE) a. Check if the post endpoint is running
	// (DONE) b. Check if the post endpoint encounters an error if there is no username
	// (DONE) c. Check if the post endpoint encounters an error if there is no age
// (DONE) 3. Add your updates to a new s3 online repo and push to git with the commit message of “Add activity code S3".
// (DONE) 4. Add the link in Boodle.