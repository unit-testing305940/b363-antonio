const {names , users} = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })  
        }

        // 1. Add a another "if statement" in the /person post route that returns and sends 400 status and error message if the request body does not have a username/alias field.
        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error' : 'Bad Request: Alias is missing property'
            })
        }
    })

    // S4 Activity Start
    app.post('/login',(req,res) => {

        let foundUser = users.find((user) => {

            return user.username === req.body.username && user.password === req.body.password

        });

        if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error' : 'Bad Request: Username is missing'
            })
        }

        if(!req.body.hasOwnProperty('password')){
            return res.status(400).send({
                'error' : 'Bad Request: Password is missing'
            })
        }

        if(foundUser){
            return res.status(200).send({
                'success' : 'Logged in successfully!'
            })
        }

        if(!foundUser){
            return res.status(403).send({
                'error' : 'Incorrect username or password.'
            })
        }
    })
    // S4 Activity End
}

//S4 Activity Instructions:

// 1. In your routes.js, add a new route provided by your instructor.
// 2. In your routes_test.js, add the new test cases for your api_test_suite_login provided by your instructor.
// 3. In your util.js, Add the new object provided by your instructor.
// 4. Check and run the api_test_suite_login.
// npm run-script specifictest "api_test_suite_login"
// 5. Identify the failing tests.
// 6. Update the route to pass the tests.
// 7. Add your updates to a new s4 online repo and push to git with the commit message of “Add activity code S4".
// 8. Add the link in Boodle.

// Stretch Goal:
// 1. Create a new test case in the api_test_suite_login to check a scenario where the user sends a request with a wrong password.
// - Expect the response status code to be 403.
// 2. Refactor the '/login' route and add a response to this scenario.
// 3. Update your online repo.