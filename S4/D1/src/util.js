function factorial(n){
	// Mini-activity
	// Refactor the factorial method to accommodate the scenario of a numerical string.
	if(typeof n !== 'number') return undefined;
	if(n<0) return undefined;
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
	// 5 * 4 * 3 * 2 * 1 = 120
}

// 2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
function div_check(n){
	//a. If the number received is divisible by 5, return true
    if(n%5 === 0) return true
    // b. If the number received is divisible by 7, return true.
    if(n%7 === 0) return true
    // Return false if otherwise
    return false

}

const names = {
	"Brandon": {
		"name": "Brandon Boyd",
		"age": 35
	},
	"Steve": {
		"name": "Steve Tyler",
		"age": 56
	}
}

// S4 Activity Start
const users = [
    {
        username: "brBoyd87",
        password: "87brandon19"

    },
    {
        username: "tylerofsteve",
        password: "stevenstyle75"
    }
]
// S4 Activity End

module.exports = {
	factorial: factorial,
	div_check: div_check,
	names: names,
	// S4 Activity
    users: users
} 

// S4 Activity Instructions:

// 1. In your routes.js, add a new route provided by your instructor.
// 2. In your routes_test.js, add the new test cases for your api_test_suite_login provided by your instructor.
// 3. In your util.js, Add the new object provided by your instructor.
// 4. Check and run the api_test_suite_login.
// npm run-script specifictest "api_test_suite_login"
// 5. Identify the failing tests.
// 6. Update the route to pass the tests.
// 7. Add your updates to a new s4 online repo and push to git with the commit message of “Add activity code S4".
// 8. Add the link in Boodle.

// Stretch Goal:
// 1. Create a new test case in the api_test_suite_login to check a scenario where the user sends a request with a wrong password.
// - Expect the response status code to be 403.
// 2. Refactor the '/login' route and add a response to this scenario.
// 3. Update your online repo.