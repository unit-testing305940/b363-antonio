const chai = require('chai');

const expect = chai.expect;
// same as const {expect} = require("chai");

const http = require('chai-http');
chai.use(http);

describe("api_test_suite", () => {

	it("test_api_people_is_running", () => {
		chai.request('http://localhost:5001')
		// "get()" specifies the type of HTTP request
		// accepts the API endpoint
		.get('/people')
		// "end()" is the method that handles the error and response that will be received from the endpoint
		.end((err, res) => {
			// ".not" negates all assertions that follow in the chain
			expect(res).to.not.equal(undefined);
		})
	})

	it("test_api_people_returns_200", (done) => {
		chai.request("http://localhost:5001")
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			// "done()" function is typically used in asynchronous test cases to signal that the test case is complete
			done();
		})
	})

	it("test_api_post_person_returns_400_if_no_person_name", (done) => {
		chai.request("http://localhost:5001")
		// ".post()" specifies the type of http request
		// accepts the API endpoint
		.post('/person')
		// "type" specifies the type of input to be sent out as part of the POST request
		.type('json')
		// "send()" specifies the data to be sent as part of the POST request
		// request body
		.send({
			alias: "Jason",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// 2. In your routes_test.js, Add following test cases in the api_test_suite:
	// a. Check if the post endpoint is running
	it("test_api_post_person_is_running", () => {
	    chai.request('http://localhost:5001')
	    .post('/person')
	    .type('json')
	    .send({
	        alias: "Jay",
	        name: "Jay White",
	        age: 27
	    })
	    .end((err, res) => {
	        expect(res).to.not.equal(undefined);
	    })
	})

	// Check if the post endpoint encounters an error if there is no alias
	it('test_api_post_person_returns_400_if_no_ALIAS', (done) => {
        chai.request('http://localhost:5001')
        .post('/person')
        .type('json')
        .send({
            name: "Jay White",
            age: 27
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    })

	// c. Check if the post endpoint encounters an error if there is no age
	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
        chai.request('http://localhost:5001')
        .post('/person')
        .type('json')
        .send({
            alias: "Jay",
            name: "Jay White",
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    })
    
})

describe("api_test_suite_login", () => {

	// S4 Activity Start
	it('test_api_post_login_returns_400_if_no_username', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			password: "wrongPW"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('test_api_post_login_returns_400_if_no_password', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	it('test_api_post_login_returns_200_if_correct_credentials', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "87brandon19",
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

	it('stretch_goal_post_login_returns_403_if_wrong_credentials', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "87brandon1999",
		})
		.end((err, res) => {
			expect(res.status).to.equal(403);
			done();
		})
	})
    // S4 Activity End
})

//S4 Activity Instructions:

// (DONE) 1. In your routes.js, add a new route provided by your instructor.
// (DONE) 2. In your routes_test.js, add the new test cases for your api_test_suite_login provided by your instructor.
// (DONE) 3. In your util.js, Add the new object provided by your instructor.
// (DONE) 4. Check and run the api_test_suite_login.
	// npm run-script specifictest "api_test_suite_login"
// 5. Identify the failing tests.
// 6. Update the route to pass the tests.
// 7. Add your updates to a new s4 online repo and push to git with the commit message of “Add activity code S4".
// 8. Add the link in Boodle.

// Stretch Goal:
// 1. Create a new test case in the api_test_suite_login to check a scenario where the user sends a request with a wrong password.
// - Expect the response status code to be 403.
// 2. Refactor the '/login' route and add a response to this scenario.
// 3. Update your online repo.